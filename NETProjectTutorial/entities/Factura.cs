﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string codigoFactura;
        private DateTime fecha;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string CodigoFactura
        {
            get
            {
                return codigoFactura;
            }

            set
            {
                codigoFactura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public Factura(int id, string codigoFactura, DateTime fecha, string observaciones, double subtotal, double iva, double total)
        {
            this.Id = id;
            this.CodigoFactura = codigoFactura;
            this.Fecha = fecha;
            this.Observaciones = observaciones;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
        }
    }
}

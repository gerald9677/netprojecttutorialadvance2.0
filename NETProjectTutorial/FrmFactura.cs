﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private double subtotal;
        private double iva;
        private double total;
        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }
        public DataSet DsSistema { set { dsSistema = value; } }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "SKUN";
            cmbProductos.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dvgProductoFactura.DataSource = bsProductoFactura;
        }

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProducto = ((DataRowView)cmbProductos.SelectedItem).Row;
            txtCantidad.Text = drProducto["Cantidad"].ToString();
            txtPrecio.Text = drProducto["Precio"].ToString();


        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
            }
            catch(ConstraintException)
            {
                MessageBox.Show(this,"ERROR, producto ya agregado, verifique por favor!","Mensaje de ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void dvgProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dvgProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura


        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrPro
        }
    }
}

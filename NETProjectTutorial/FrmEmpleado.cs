﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {
        private DataTable dtEmpleados;
        private DataRow drEmpleado;

        public FrmEmpleado()
        {
            InitializeComponent();
        }

        public DataTable DtEmpleados { set { dtEmpleados = value; } }

        public DataRow DrEmpleado { set { drEmpleado = value; } }

        private void btnNew_Click(object sender, EventArgs e)
        {
            string cedula, inss, nombres, apellidos, direccion, tconvencional, tcelular;
            Empleado.Sexo sexo;
            double salario;

            cedula = msktCedula.Text;
            inss = msktInss.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            tconvencional = msktTelConv.Text;
            tcelular = msktTelCel.Text;
            sexo = Empleado.Sexo.MALE;
            salario = double.Parse(msktSalario.Text);

            if(drEmpleado != null)
            {

            } else
            {
                int id = dtEmpleados.Rows.Count + 1;
                dtEmpleados.Rows.Add(id, cedula, inss, nombres, apellidos, direccion, tconvencional, tcelular, sexo, salario);
                MessageBox.Show("El empleado se ha agregado satisfactoreamente!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            cmbSexo.DataSource = Enum.GetValues(typeof(Empleado.Sexo));
        }
    }
}
